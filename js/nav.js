(function() {
    'use strict';

    var currentPaneId = null;
    var currentSelectItem = null;

    
    var _hideRelatedPanel = function(id, _callback){
         $('#' + id).animate({ opacity: 0 }, 200, 'swing', _callback);
    }

    var _showRelatedPanel = function(id){
        var relatedPanel = document.getElementById(id),
        parentNode = relatedPanel.parentNode;
        relatedPanel.style.opacity = 0;

        parentNode.insertBefore(relatedPanel, parentNode.firstChild);
        $('#' + id).animate({ opacity: 1 }, 200);
    }

    var _click = function(id){
        if(currentSelectItem){
            currentSelectItem.classList.remove("active");
            currentSelectItem = null;
        }
        if(currentPaneId != id){
            this.classList.add("active");
            currentSelectItem = this;

            if(currentPaneId){
                _hideRelatedPanel(currentPaneId, function(){
                    _showRelatedPanel(id);
                });
            }
            else{
                _showRelatedPanel(id);
            }
            currentPaneId = id;
        }
        else{
            currentPaneId = null;
        }
        currentSelectItem ? $('#dropdown').slideDown(): $('#dropdown').slideUp();
    }

    var list = document.querySelectorAll("[data-related-panel]");
        list.forEach(function(item){
        item.addEventListener('click', _click.bind(item, item.getAttribute("data-related-panel")));
    });
})();
