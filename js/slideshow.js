(function() {
    'use strict';

    var slideshow = [{
        "image": "images/gallery_1.jpeg",
        "caption": "Cloudy with a chance of moon"
        }, {
        "image": "images/gallery_2.jpeg",
        "caption": "Half moon mountain"
        }, {
        "image": "images/gallery_3.jpeg",
        "caption": "Moonrise"
    }];


    var length = slideshow.length,
        index = 0,
        wrapper = document.querySelector("#gallery .gallery-wrapper"),
        btn_prev = document.querySelector("#gallery #prev"),
        btn_next = document.querySelector("#gallery #next"),
        caption = document.querySelector("#gallery #caption"),
        imagesNode = [];

    var _str = "";
    slideshow.forEach(function(itemData, i){
        _str += '<img src="' + itemData.image + '"' +
            ' alt ="' + itemData.caption + '"' + 
            ' title ="' + itemData.caption + '"' +
            (i == 0 ? 'class="current"' : "") +
            '>';

        i == 0 && (caption.innerHTML = itemData.caption);
    });
    var _div = document.createElement('div');
    _div.innerHTML = _str;

    while(_div.childNodes.length){
        var item = _div.firstChild;
        imagesNode.push(item);
        wrapper.appendChild(item);
    }

    btn_prev.disabled = true;

    var _click = function(isPrev){
        imagesNode[index].classList.remove('current');
        imagesNode[isPrev ? --index : ++index].classList.add('current');
        caption.innerHTML =  imagesNode[index].title;
        btn_prev.disabled = index == 0;
        btn_next.disabled = index == length - 1;
    }

    btn_prev.addEventListener("click", _click.bind(null, true));
    btn_next.addEventListener("click", _click.bind(null, false));
})();