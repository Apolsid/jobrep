(function() {
    'use strict';

    var jsonURL = 'https://credentials-api.generalassemb.ly/explorer/posts';

    var offset = 4;
    var articleListNode = document.getElementById("article-list");
    var btnNode = document.getElementById("load-more");
    var footer = btnNode.parentNode;
    var template = '<article>' +
        '<i class="fa {category}"></i>' +
        '<h2>From the Archive</h2>' +
        '<h1>{title}</h1>' +
        '<h3>{date}</h3>' +
        '<p>{blurb}</p>' +
        '</article>';


    var setState = function(type){
        switch(type){
            case "normal":
                btnNode.disabled = false;
                btnNode.innerHTML = 'Load More';
                break;
            case "load":
                btnNode.disabled = true;
                btnNode.innerHTML = 'Exploring the Archive <i class="fa fa-circle-o-notch fa-spin"></i>';
                break;
            case "end":
                btnNode.disabled = true;
                btnNode.innerHTML = 'End of Archive';
                break;
            case "error":
                btnNode.disabled = true;
                btnNode.innerHTML = 'Something Went Wrong <i class="fa fa-exclamation-triangle"></i>';
                break;
        }
    }

    var callback = function(data, textStatus){
        if(textStatus == "success"){
            var posts = data.posts;
            setState(posts.length == 4 ? "normal" : "end");

            posts.forEach(function(item){
                var _str = template;
                for(var key in item)
                    _str = _str.replace('{' + key + '}', item[key]);

                var _div = document.createElement('div');
                _div.innerHTML = _str;

                while(_div.childNodes.length)
                    articleListNode.insertBefore(_div.firstChild, footer);
            });

            offset += posts.length;
        }
        else{
            setState("error");
        }
    }

    btnNode.addEventListener('click', function(){
        setState("load");
        $.get(jsonURL, { offset: offset }, callback, "json");
    });
})();